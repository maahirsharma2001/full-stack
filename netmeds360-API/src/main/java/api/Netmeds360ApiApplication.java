package api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Netmeds360ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Netmeds360ApiApplication.class, args);
	}

}
