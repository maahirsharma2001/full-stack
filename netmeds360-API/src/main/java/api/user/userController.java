package api.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
@RestController
public class userController {
    @Autowired
    userRepo userRepo;

    @RequestMapping("/")
    public status check () {
        return status.SUCCESS;
    }
    @PostMapping("/users/register")
    public @Valid userModel registeruser(@Valid @RequestBody userModel newuser) {
        List<userModel> users = userRepo.findAll();
        System.out.println("New user: " + newuser.toString());
        for (userModel user : users) {
            System.out.println("Registered user: " + newuser.toString());
            if (user.equals(newuser)) {
                System.out.println("user Already exists!");
                return newuser;
            }
        }
        userRepo.save(newuser);
        return newuser;
    }
    @PostMapping("/users/login")
    public @Valid userModel loginuser(@Valid @RequestBody userModel user) {
        List<userModel> users = userRepo.findAll();
        for (userModel other : users) {
            if (other.equals(user)) {
                user.setLoggedIn(true);
                userRepo.save(user);
                return user;
            }
        }
        return new userModel();
    }
    @PostMapping("/users/logout")
    public userModel loguserOut(@Valid @RequestBody userModel user) {
        List<userModel> users = userRepo.findAll();
        for (userModel other : users) {
            if (other.equals(user)) {
                user.setLoggedIn(false);
                userRepo.save(user);
                return user;
            }
        }
        return new userModel();
    }
    @DeleteMapping("/users/all")
    public status deleteusers() {
        userRepo.deleteAll();
        return status.SUCCESS;
    }
}
